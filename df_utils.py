import pandas as pd
import numpy as np
import json
from pprint import pprint

def defSeason(month):
    if (month in [12,1,2]):
        return 0
    elif (month in [3,4,5]):
        return 1
    elif (month in [6,7,8]):
        return 2
    elif (month in [9,10,11]):
        return 3
    else:
        return None

def filter_df(df_src, params):
    df = df_src
    if ('payer' in params and params['payer']):
        df = df.loc[df['payer'] == params['payer']]
        if not df.size:
            valid_prm = df_src['payer'].unique().tolist()
            raise Exception({'error':'bad payer param - no transactions with such payer in dataset, so no history for prediction', 'available_payers': valid_prm })
        if ('recipient' in params and params['recipient']):
            valid_prm = df['recipient'].unique().tolist()
            df = df.loc[df['recipient'] == params['recipient']]
            if not df.size:
                raise Exception({'error':'bad recipient param - no transactions with such payer+recipient in dataset, so no history for prediction', 'payer': params['payer'], 'available_recipient': valid_prm })

    if not df.size:
        raise Exception('dataset is empty, nothing to analyse')

    return df

def mark_dataset(df_src):
    target_columns_map_dict = {
        'Исходная дата платежа': 'date',
        'Сумма платежа': 'sum',
        'Статья': 'isCoreActivity',
        'Организация': 'payer',
        'Контрагент' : 'recipient',
        'Назначение платежа': 'purpose',
    }
    df = df_src[target_columns_map_dict.keys()]
    df = df.rename(columns=target_columns_map_dict)
    
    activity_map_dict = {
        'текущая деятельность': True, 
        'не текущая деятельность': False,
    }
    df.replace({"isCoreActivity": activity_map_dict}, inplace=True)
    
    companies_map_ids = { 
        'ООО "Финансист"': 1,
        'ООО "Дельта Вэй"' : 2,
        'ИП Кутузова И. Л.' :  3,
        'ООО "Дельта Вэй программные решения"' : 4,
        'Личные счета' : 5,
        'ИП Сухачев Н. С.' : 6,
        'ИП Тешебаев А. Ш.' : 7
    }
    df.replace({"payer": companies_map_ids}, inplace=True)
    
    df['sum'] =  df['sum'].apply(pd.to_numeric)
    df['date'] = pd.to_datetime(df['date'], format='%d.%m.%Y %H:%M')
    df['dow'] = df['date'].dt.dayofweek  # Monday=0
    df['month'] = df['date'].dt.month    # January=1, December=12
    df['season'] = df['month'].apply(defSeason) # winter=0
    return df

# def get_extended_orders_df(df_src):
#     startdate = '2020-01-01'
#     date_stat = df_src['date'].value_counts()
#     date_stat_df = pd.DataFrame({'date': date_stat.index,'orders_n':date_stat.values})
#     date_stat_df['date'] = pd.to_datetime(date_stat_df['date'])
#     date_stat_df = date_stat_df.set_index(pd.DatetimeIndex(date_stat_df['date']))
#     dti = pd.date_range(startdate, periods=365, freq="D")
#     date_stat_df = date_stat_df.reindex(dti)
#     date_stat_df['date'].fillna( date_stat_df.index.to_series(), inplace=True)
#     date_stat_df['orders_n'] = date_stat_df['orders_n'].fillna(0).astype(np.int64)
#     date_stat_df = date_stat_df.drop(columns=['date'])
#     return date_stat_df
    
def get_extended_orders_df(df_src, predictprm):
    startdate = df_src['date'].min()
    date_stat = df_src['date'].value_counts()
    date_stat_df = pd.DataFrame({
        'date': date_stat.index,
        'transactions': date_stat.values,
        #'core': df_src.loc[df_src['isCoreActivity'] == 1].loc[df_src['date'] == date_stat.index],
        #'nocore': df_src.loc[df_src['isCoreActivity'] == 0].loc[df_src['date'] == date_stat.index],
    })    
    date_stat_df['date'] = pd.to_datetime(date_stat_df['date'])
    date_stat_df['core'] = df_src.loc[df_src['isCoreActivity'] == 1].loc[df_src['date'] == date_stat_df['date']],
    date_stat_df['nocore'] = df_src.loc[df_src['isCoreActivity'] == 0].loc[df_src['date'] == date_stat_df['date']],
    
    date_stat_df = date_stat_df.set_index(pd.DatetimeIndex(date_stat_df['date']))
    dti = pd.date_range(startdate, periods=365, freq="D")
    date_stat_df = date_stat_df.reindex(dti)
    date_stat_df['date'].fillna( date_stat_df.index.to_series(), inplace=True)
    date_stat_df['orders_n'] = date_stat_df['orders_n'].fillna(0).astype(np.int64)
    date_stat_df = date_stat_df.drop(columns=['date'])
    return date_stat_df
