#!/usr/bin/env bash
INPUT_ARGS="$@"
# IMAGE=pavelsr/finansist-ml
IMAGE=pavelsr/guru-core

docker run -u $(id -u):$(id -g) --rm -v $(pwd):/data -w /data $IMAGE bash -c "python predict.py ${INPUT_ARGS}"
# add -it options if needed
