#!/usr/bin/env python

import sys
import click
import pandas as pd
import numpy as np
import json

from neuro import predict
from df_utils import mark_dataset, filter_df, get_extended_orders_df

from pprint import pprint

@click.command()
# @click.argument('days')
@click.option('--file', '-f',  default='Payment.csv', show_default=True, help='Location of dataset csv file', type=click.Path(exists=True))
@click.option('--days', '-d',  type=int, default=1, show_default=True, help='Amount of days in forecast')
@click.option('--iwidth', '-i', type=int, default=28, show_default=True, help='How many days of history to take into consideration (input_width of WindowGenerator)',)
@click.option('--units', '-u', type=int, default=32, show_default=True, help='Positive integer, dimensionality of the output space at tf.keras.layers.LSTM')
@click.option('--toint', default=False, show_default=True, help='Make data integer', is_flag=True)

# Известные параметры транзакции, влияющие на прогноз
@click.option('--date', type=str, show_default=True, help='Дата, в которую будет совершён платеж, в формете ISO 8601')
@click.option('--sum', type=str, show_default=True, help='Сумма платежа')
@click.option('--payer', type=str, show_default=True, help='Отправитель платежа (название организации)')
@click.option('--recipient', type=str, show_default=True, help='Получатель платежа (название организации)')
@click.option('--purpose', type=str, show_default=True, help='Назначение платежа')

# Параметр, который хотим предсказать
@click.option('--predictprm', type=str, default='isCoreActivity', show_default=True, 
    help='Какой параметр предсказывать, по умолчанию - категория статьи расходов (основные/неосновные)')

# --payer 1 --predictprm isCoreActivity
 
def main(file, days, iwidth, units, toint, date, sum, payer, recipient, purpose, predictprm):
    # print('Start main')
    df = pd.read_csv( file, sep='\t', encoding='utf-16', decimal="," )
    # print('read_csv ok')
    df = mark_dataset(df)
    
    # TO-DO: add paymentPurpose
    filter_params = { "payer":payer,"recipient":recipient }

    try:
        df = filter_df(df,filter_params)
        #print(df)
    except Exception as e:
        return click.echo( json.dumps(e.args[0]) )

    df = get_extended_orders_df(df, predictprm)
    print(df)

    # column_indices = {name: i for i, name in enumerate(df.columns)}
    predictions, train_mean, train_std = predict(df,days,iwidth,units)

    # pprint(history.history['mean_squared_error'])
    # pprint(history.history['mean_absolute_percentage_error'])
    # pprint(history.history['val_loss'])

    if toint:
        predictions = [int(a) for a in predictions]
    else:
        predictions = np.float64(predictions).tolist()

    return click.echo( json.dumps({ "predictprm":predictprm, "predictions":predictions, "train_mean": train_mean[predictprm], "train_std": train_std[predictprm] }))
    # return click.echo( json.dumps({ "predictions":predictions, "train_meanx": train_mean.predict, "train_std": train_std.predict }))
    # return click.echo( json.dumps({ "predictions":predictions, "train_mean": train_mean.orders_n, "train_std": train_std.orders_n }))


if __name__ == "__main__":
    main()
